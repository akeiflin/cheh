#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include "exec.h"

t_command *head_cmd;

char **mallocarg(int i, ...)
{
	char **tab;
	va_list argp;
  	va_start(argp, i);
	tab = calloc(sizeof(char *) * i + 1, sizeof(char *) * i + 1);
	for(int j = 0; j < i; j++)
        tab[j] = (char *)va_arg(argp, char *);
    va_end(argp);
	return tab;
}

t_command *create_cmd(char *name, char **args, t_redirec *redir, char **env)
{
	t_command *cmd;

	cmd = calloc(sizeof(t_command), sizeof(t_command));
	cmd->program = name;
	cmd->args = args;
	cmd->redirections = redir;
	cmd->infd = 0;
	cmd->outfd = 1;
	cmd->errfd = 2;
}

void	addpipecmd(char *name, char **args, t_redirec *redir, char **env)
{
	t_command *cmd;

	for (cmd = head_cmd; cmd->piped; cmd = cmd->piped);
	cmd->piped = create_cmd(name, args, redir, env);
}

int	main(int argc, char *argv[], char *env[])
{
	char **arg1 = mallocarg(2, "ls", ".");
	t_redirec *redir1 = malloc(sizeof(t_redirec));
	*redir1 = (t_redirec){.next = NULL, .type = '<', .left = 0, .right = 1, .outfile = "test"};
	head_cmd = create_cmd(
		arg1[0], 
		arg1, 
		NULL,//redir1, 
		env);

	char **arg2 = mallocarg(2, "cat", "-e");
	t_redirec *redir2 = malloc(sizeof(t_redirec));
	*redir2 = (t_redirec){.next = NULL, .type = '<', .left = 0, .right = 2, .outfile = "test"};
	addpipecmd(
		arg2[0],
		arg2,
		redir2,
		env
	);/*
	addpipecmd(
		arg2[0],
		arg2,
		NULL,
		env
	);*/
	exec_boucle(head_cmd, env);
	return (0);
}