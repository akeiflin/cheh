#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <stddef.h>
#include <fcntl.h>
#include <term.h>
#include "exec.h"

void	set_redirections(t_command *cmd)
{
	t_redirec	*redir;
	int			tmpfd;
	int		notruc;

	notruc = O_CREAT | O_RDWR | O_APPEND;
	redir = cmd->redirections;
	while (redir)
	{
		if (redir->type == '>' || redir->type == ')')
		{
			if (redir->left == STDOUT)
			{
				if (redir->outfile)
					cmd->outfd = open(redir->outfile, 
						((redir->type == ')') ? notruc : notruc - 512), 0644);
				else
					cmd->outfd = redir->right;
			}
			if (redir->left == STDERR)
			{
				if (redir->outfile)
					cmd->errfd = open(redir->outfile, 
						((redir->type == ')') ? notruc : notruc + 8), 0644);
				else
					cmd->errfd = redir->right;
			}
		}
		if (redir->type == '<')
			if (redir->left == 0)
				if (redir->outfile != NULL)
					cmd->infd = open(redir->outfile, O_RDONLY);
		redir = redir->next;
	}
}

void	set_all_redir(t_command *head_cmd)
{
	t_command *cmds;

	cmds = head_cmd;
	while (cmds)
	{
		if (cmds->redirections)
			set_redirections(cmds);
		if (cmds->piped)
			set_all_redir(cmds->piped);
		cmds = cmds->next_command;			
	}
}

int		exec_it(t_command *cmd, int infd, int outfd, char **env)
{
	//or (int i = 1; i; i);
	if (infd != STDIN)
    {
		dup2(infd, STDIN);
    	close(infd);
    }
	if (outfd != STDOUT)
    {
		dup2(outfd, STDOUT);
		close(outfd);
    }
	if (cmd->errfd != STDERR)
		dup2(cmd->errfd, STDERR);
	execve(cmd->program, cmd->args, env);
	exit(1);
}

int		exec_boucle(t_command *head_cmd, char **env)
{
	t_command *cmd;
	pid_t	pid;
	int		pipefd[2] = {-1, -1};
	int		infd;
	int		outfd;

	set_all_redir(head_cmd);
	cmd = head_cmd;
	infd = cmd->infd;
	while (cmd)
	{
		if (pipefd[READ_END] != -1 && cmd->infd == STDIN)
			infd = pipefd[READ_END];
		else
			infd = cmd->infd;
		outfd = cmd->outfd;
		if (cmd->piped)
		{
			if (pipe(pipefd) == -1)
				return (-1);
			if (cmd->outfd == STDOUT)
				outfd = pipefd[WRITE_END];
		}
		else
			outfd = cmd->outfd;
		if ((pid = fork()) < 0)
			return (-1);
		else if (pid == 0) //CHILD
			exec_it(cmd, infd, outfd, env);
		//PARRENT
		if (infd != STDIN)
			close(infd);
		if (outfd != STDOUT)
			close(outfd);
		cmd = cmd->piped;
	}
	wait(0);
	return (0);
}