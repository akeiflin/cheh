#include <sys/types.h>
#include <termios.h>
#ifndef NULL
#	define NULL		(void *)0
#endif
#define READ_END	0
#define WRITE_END	1
#ifndef STDIN
# define STDIN		0
# define STDOUT		1
# define STDERR		2
#endif

typedef struct		s_redirec
{
	struct s_redirec	*next;
	char				type;
	int					left;
	int					right;
	char				*outfile;
}					t_redirec;

typedef struct		s_command
{
	char				*program;
	char				**args;
	t_redirec			*redirections;
	struct s_command	*piped;
	struct s_command	*next_command;
	int					infd;
	int					outfd;
	int					errfd;

}					t_command;

int		exec_boucle(t_command *head_cmd, char **env);