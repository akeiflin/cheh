/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   double_linked_list.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akeiflin <akeiflin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/19 16:27:25 by akeiflin          #+#    #+#             */
/*   Updated: 2019/10/19 17:13:12 by akeiflin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"
#include "truc.h"

/*
**  Add a new node on the beggining of a list
**  Return 0 if succes and -1 if error
*/

int push_node(t_node** head_ref, void *new_data) 
{ 
	t_node	*new_node;

    if (!(new_node = malloc(sizeof(t_node))))
        return (-1);
	new_node->data = new_data; 
	new_node->next = *head_ref; 
	new_node->prev = NULL; 
	if (*head_ref != NULL) 
		(*head_ref)->prev = new_node; 
	*head_ref = new_node;
    return (0);
} 

/*
**  Add a new node before 'next_node'
**  Return 0 if succes and -1 if error
*/

int insertBefore(t_node** head_ref, t_node* next_node, void *new_data) 
{
    t_node	*new_node;

	if (next_node == NULL)
		return (-1);
	if (!(new_node = ft_calloc(sizeof(t_node))))
        return (-1);
	new_node->data = new_data;
	new_node->prev = next_node->prev;
	next_node->prev = new_node;
	new_node->next = next_node;
	if (new_node->prev != NULL) 
		new_node->prev->next = new_node; 
	else
		*head_ref = new_node; 
	return (0);
}

/*
**  Add a new node on the end of a list
**  Return 0 if succes and -1 if error
*/

int append_node(t_node** head_ref, void *new_data)
{
	t_node	*new_node;
	t_node	*nav_node;

	nav_node = *head_ref;
	while (nav_node && nav_node->next)
		nav_node = nav_node->next;
	if (!(new_node = ft_calloc(sizeof(t_node))))
		return (-1);
	new_node->data = new_data;
	if (nav_node)
	{
		nav_node->next = new_node;
		new_node->prev = nav_node;
	}
	else
		*head_ref = new_node;
	return (0);
}