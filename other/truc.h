/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   truc.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akeiflin <akeiflin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/19 16:25:37 by akeiflin          #+#    #+#             */
/*   Updated: 2019/11/26 22:20:20 by akeiflin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

typedef struct  s_node
{
    void            *data;
    struct s_node   *next;
    struct s_node   *prev;
}               t_node;
typedef t_node t_historic;

int				push_node(t_node** head_ref, void *new_data);
int				insertBefore(t_node** head_ref, t_node* next_node,
								void *new_data);
int             append_node(t_node** head_ref, void *new_data);

void			add_historic(char *command);
char			*historic_get_next(void);
char			*historic_get_last(void);
void			historic_reset(void);
int				historic_on_use(int act);