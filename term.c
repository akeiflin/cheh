#include <term.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include "libft.h"
#include "ligne.h"

void	clear_line(t_line *line)
{
	t_pos curpos;
	t_winsize termsize;
	int startline;

	termsize = get_winsize();
	curpos = get_cur_pos();
	startline = curpos.y - (line->index + line->prompt->size) / termsize.ws_col;
	cur_mov(line->prompt->size, startline - 1);
	term_other(TERM_CLEAR_CUR_END, 1);
}

void	cur_mov(int y, int x)
{
	char *buff;

	buff = tgetstr("cm", NULL);
	tputs(tgoto(buff, y, x), 1, &ft_putchar);
}

t_pos	get_cur_pos()
{
	t_pos	pos;
	char buff[20] = {'\0'};
	int i;
	int readsize;

	write(1, "\033[6n", 4);
	readsize = read(0 ,buff ,19);
	i = 2;
	pos.y = ft_atoi(&buff[i]);
	while (i < 20 && buff[i++] != ';');
	pos.x = ft_atoi(&buff[i]);
	return (pos);
}

void	term_other(char *str, int iter)
{
	char *buff;

	buff = tgetstr(str, NULL);
	if (buff)
	{
		while (iter--)
			tputs(buff, 1, &ft_putchar);
	}
}

t_winsize get_winsize()
{
	t_winsize size;

	ioctl(0, TIOCGWINSZ, &size);
	return size;
}

void 	cur_move_to_index(t_line *line, int index)
{
	t_pos	cur_pos;
	t_winsize winsize;
	int x;
	int y;

	cur_pos = get_cur_pos();
	winsize = get_winsize();
	y = cur_pos.y + (((index + line->prompt->size - 1) / winsize.ws_col) - ((line->index + line->prompt->size - 1) / winsize.ws_col)) - 1;
	if ((line->index + line->prompt->size) % winsize.ws_col == 0)
		--y;
	if ((index + line->prompt->size) % winsize.ws_col == 0)
		++y;
	if (y == winsize.ws_row)
	{
		--y;
		term_other(TERM_END, 1);
		term_other(TERM_SCROLL_UP, 1);
	}
	x = (index  + line->prompt->size) % winsize.ws_col;
	line->index = index;
	cur_mov(x, y);
}