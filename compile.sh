#!/bin/bash
if [ "${TERM_PROGRAM}" = "iTerm.app" ]
then
    gcc ligne.c misc.c term.c  other/double_linked_list.c other/historique.c libft/libft.a -Ilibft/includes -ltermcap -g -D __ITERM__ $1 $2 $3 $4 $5 $6 $7 $8 $9
else
    gcc ligne.c misc.c term.c  other/double_linked_list.c other/historique.c libft/libft.a -Ilibft/includes -ltermcap -g $1 $2 $3 $4 $5 $6 $7 $8 $9
fi
