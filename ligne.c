#include <termios.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <term.h>
#include "libft.h"
#include "ligne.h"
#include "other/truc.h"

void	write_on_line(t_line *line, int readsize, char *buff)
{
	if (ft_strlen(line->line) + readsize > line->alloczise)
		expend_line_alloc(line, readsize);
	ft_insert_in_line(line->line, buff, line->index);
	line->index += readsize;
	ft_putstr(buff);
	if ((line->index + line->prompt->size) % get_winsize().ws_col == 0)
	{
		term_other(TERM_DOWN, 1);
		term_other(TERM_LEFT, (int)get_winsize().ws_col - 1);
	}
	if (line->line[line->index])
	{
		term_other(TERM_SAVE_CUR, 1);
		ft_putstr(&line->line[line->index]);
		term_other(TERM_LOAD_CUR, 1);
	}
}

void	arrow_left_act(t_line *line)
{ //Attention a modif pour le final push pour que iterm (juste laisser un TERM_LEFT)
	t_pos	pos;

	if (line->index > 0)
	{
		pos = get_cur_pos();
		if (pos.x == 1 && pos.y == 1)
			term_other(TERM_SCROLL_DOWN, 1);
		if (pos.x == 1)
			cur_mov(get_winsize().ws_col - 1, get_cur_pos().y - 2);
		else
			term_other(TERM_LEFT, 1);
		--line->index;
	}
}

void	arrow_right_act(t_line *line)
{ //Attention a modif pour le final push pour que iterm (juste laisser un TERM_LEFT)
	t_pos	pos;
	
	if (line->index < ft_strlen(line->line))
	{
		pos = get_cur_pos();
		if (pos.y == get_winsize().ws_col + 1)
			term_other(TERM_SCROLL_UP, 1);
		if (pos.x == get_winsize().ws_col)
			cur_mov(0, get_cur_pos().y);
		else
			term_other(TERM_RIGHT, 1);
		++line->index;
	}
}

void	arrow_up_act(t_line *line)
{
	char		*historic;
	t_prompt	*save;

	if ((historic = historic_get_next()))
	{
		clear_line(line);
		save = line->prompt;
		free(line->line);
		init_line(line, ft_strdup(historic), ft_strlen(historic), ft_strlen(historic));
		line->prompt = save;
		ft_putstr(line->line);
		if ((line->index + line->prompt->size) % get_winsize().ws_col == 0)
		{
			term_other(TERM_DOWN, 1);
			term_other(TERM_LEFT, (int)get_winsize().ws_col - 1);
		}
	}
}

void	arrow_down_act(t_line *line)
{
	char	*historic;
	t_prompt	*save;

	if (historic_on_use(0))
	{
		save = line->prompt;
		clear_line(line);
		if ((historic = historic_get_last()))
		{
			free(line->line);
			init_line(line, ft_strdup(historic), ft_strlen(historic), ft_strlen(historic));
			ft_putstr(line->line);
			if ((line->index + save->size) % get_winsize().ws_col == 0)
			{
				term_other(TERM_DOWN, 1);
				term_other(TERM_LEFT, (int)get_winsize().ws_col - 1);
			}
		}
		else
		{
			free(line->line);
			init_line(line, ft_calloc(sizeof(char) * (BUFFSIZE + 1)), BUFFSIZE, 0);
			historic_reset();
		}
		line->prompt = save;
	}
}

void	arrow_line_action(int arrow, t_line *line)
{
	if (arrow == KEY_LEFT)
		arrow_left_act(line);
	else if (arrow == KEY_RIGHT)
		arrow_right_act(line);
	else if (arrow == KEY_UP)
		arrow_up_act(line);
	else if (arrow == KEY_DOWN)
		arrow_down_act(line);
	else if (arrow == KEY_CTRL_LEFT)
		cur_move_to_index(line, last_word_index(line));
	else if (arrow == KEY_CTRL_RIGHT)
		cur_move_to_index(line, next_word_index(line));
}

void	del_char(t_line *line)
{
	if (line->index > 0)
	{
		ft_strcpy(&line->line[line->index - 1], &line->line[line->index]);
		cur_move_to_index(line, line->index - 1);
		term_other(TERM_SAVE_CUR, 1);
		ft_putstr(&line->line[line->index]);
		term_other(TERM_CLEAR_CUR_END, 1);
		term_other(TERM_LOAD_CUR, 1);
	}
}

t_line	*read_loop(t_prompt *prompt)
{
	char	buff[BUFFSIZE + 1];
	t_line	line = {NULL, BUFFSIZE, 0};
	int		readsize;

	line.prompt = prompt;
	line.line = ft_calloc(sizeof(char) * (BUFFSIZE + 1));
	while (!!!!!!!!!!!!!!!!!!!!!!!0)
	{
		ft_bzero(buff, sizeof(char) * BUFFSIZE + 1);
		readsize = read(STDIN_FILENO, buff, BUFFSIZE);
		if (ft_isallprint(buff))
			write_on_line(&line, readsize, buff);
		else if (is_arrow(buff))
			arrow_line_action(is_arrow(buff), &line);
		else if (ft_strcmp(buff, "\n") == 0) //validation
			break ;
		else if (ft_strcmp(buff, KEY_SUPR) == 0)
			del_char(&line);
		else if (ft_strcmp(buff, KEY_HOME) == 0)
			cur_move_to_index(&line, 0);
		else if (ft_strcmp(buff, KEY_END) == 0)
			cur_move_to_index(&line, ft_strlen(line.line));
	}
	ft_putchar('\n');
	return (ft_memdup(&line, sizeof(t_line)));
}

int	look_for_another_line(t_node *head)
{
	char	finded;
	int		i;
	char	*line;

	i = 0;
	finded = 0;
	while (head)
	{
		line = ((t_line *)head->data)->line;
		while (line[i])
		{
			if (line[i] == '"' || line[i] == '\'')
			{
				if (finded == 0)
					finded = line[i];
				else if (finded == line[i])
					finded = 0;
			}
			++i;
		}
		i = 0;
		head = head->next;
	}
	return (finded);
}

t_prompt *new_prompt(char *str)
{
	t_prompt *prompt;

	prompt = malloc(sizeof(t_prompt));
	prompt->str = ft_strdup(str);
	prompt->size = ft_strlen(prompt->str);
	return (prompt);
}

char	*concat_lines(t_node *head)
{
	char	*str;

	str = NULL;
	if (!head->next)
		return (ft_strdup(((t_line *)head->data)->line));
	while (head->next)
	{
		str = ft_strljoin(str, ((t_line *)head->data)->line, FIRST);	
		str = ft_strljoin(str, "\n", FIRST);
		head = head->next;
	}
	str = ft_strljoin(str, ((t_line *)head->data)->line, FIRST);
	return (str);
}

int main ()
{
	t_prompt	*prompt;
	t_node		*head;
	t_line		*line;
	char		*cmd;

	init_term();
	while (1)
	{
		prompt = new_prompt("42> ");
		ft_putstr(prompt->str);
		line = read_loop(prompt);
		append_node(&head, line);
		while (look_for_another_line(head))
		{
			prompt = new_prompt("> ");
			ft_putstr(prompt->str);
			line = read_loop(prompt);
			append_node(&head, line);
		}
		cmd = concat_lines(head);
		add_historic(cmd);
		historic_reset();
		head = NULL; //LEAAAAAAK
	}
	return (0);
}
