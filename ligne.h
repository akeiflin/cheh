# include <sys/ioctl.h>
# define BUFFSIZE				10
# define KEY_UP 				1
# define KEY_DOWN				2
# define KEY_LEFT				3
# define KEY_RIGHT				4

# define KEY_CTRL_LEFT			5
# define KEY_CTRL_RIGHT			6

# if defined(__ITERM__)
#  define KEY_CTRL_LEFT_CODE	"\x1b\x1b[D"
#  define KEY_CTRL_RIGHT_CODE	"\x1b\x1b[C"
# elif defined(__APPLE__)
#  define KEY_CTRL_LEFT_CODE	"\x1b""b"
#  define KEY_CTRL_RIGHT_CODE	"\x1b""f"
# else
#  define KEY_CTRL_LEFT_CODE	"\033[1;5D"
#  define KEY_CTRL_RIGHT_CODE	"\033[1;5C"
# endif

# define KEY_SUPR				"\177"
# define KEY_HOME				"\e[H"
# define KEY_END				"\e[F"

# define TERM_LEFT				"le"
# define TERM_RIGHT				"nd"
# define TERM_UP				"up"
# define TERM_DOWN				"do"
# define TERM_STARTLINE			"cr"
# define TERM_SAVE_CUR			"sc"
# define TERM_LOAD_CUR			"rc"
# define TERM_CLEAR_CUR_END		"cd"
# define TERM_CLEAR_CUR_LINE	"ce"
# define TERM_SCROLL_UP			"sf"
# define TERM_SCROLL_DOWN		"sr"
# define TERM_END				"ll"

typedef struct winsize t_winsize;
typedef struct	s_pos
{
	int	x;
	int	y;
}				t_pos;
typedef struct	s_prompt
{
	char	*str;
	int		size;
}				t_prompt;
typedef struct	s_line
{
	char		*line;
	size_t		alloczise;
	int			index;
	t_prompt	*prompt;
}				t_line;

t_line			*read_loop(t_prompt *prompt);

void			cur_mov(int y, int x);
t_pos			get_cur_pos();
void			term_other(char *str, int iter);
t_winsize		get_winsize();
void			cur_move_to_index(t_line *line, int index);

struct termios	*save_term(void);
int				init_term(void);
int				ft_isallprint(char *str);
void			expend_line_alloc(t_line *line, int readsize);
int				is_arrow(char *buff);
void			ft_insert_in_line(char *line, char *buff, int index);
void			init_line(t_line *tline, char *line, size_t allocsize, int index);
void			clear_line();
int				last_word_index(t_line *line);
int				next_word_index(t_line *line);