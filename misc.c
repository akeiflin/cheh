#include <termios.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <term.h>
#include "libft.h"
#include "ligne.h"

struct termios	*save_term(void)
{
	static int				i = 0;
	static struct termios	save;
	char					*term;

	if (i == 0)
	{
		term = getenv("TERM");
		if (term == NULL)
			return (NULL);
		if (tgetent(NULL, term) < 0)
			return (NULL);
		if (tcgetattr(STDIN_FILENO, &save) == -1)
			return (NULL);
		++i;
	}
	return (&save);
}

int				init_term(void)
{
	struct termios	*s_termios;
	char			*buff;
	char			*term;

	term = getenv("TERM");
	if (!term || ft_strcmp(term, "xterm-256color") != 0)
		return (-22);
	if (!isatty(STDIN_FILENO))
		return (-20);
	s_termios = save_term();
	if (s_termios == NULL)
		return (-21);
	s_termios->c_lflag &= ~(ICANON | ECHO);
	if (tcsetattr(STDIN_FILENO, 0, s_termios) == -1)
		return (-4);
	return (1);
}

int				ft_isallprint(char *str)
{
	int	i;

	i = 0;
	while (str[i])
	{
		if (!ft_isprint(str[i]))
			return (0);
		++i;
	}
	return (1);
}

void			expend_line_alloc(t_line *line, int readsize)
{
	char	*new_str;
	size_t		new_size;

	if (readsize > BUFFSIZE)
		new_size = line->alloczise + readsize;
	else
		new_size =  line->alloczise + BUFFSIZE;
	new_str = ft_calloc(sizeof(char) * (new_size + 1));
	ft_strcpy(new_str, line->line);
	free(line->line);
	line->line = new_str;
	line->alloczise = new_size;
}

int				is_arrow(char *buff)
{
	if (ft_strcmp(buff, "\033[A") == 0)
		return (KEY_UP);
	else if (ft_strcmp(buff, "\033[B") == 0)
		return (KEY_DOWN);
	else if (ft_strcmp(buff, "\033[C") == 0)
		return (KEY_RIGHT);
	else if (ft_strcmp(buff, "\033[D") == 0)
		return (KEY_LEFT);
	else if (ft_strcmp(buff, KEY_CTRL_LEFT_CODE) == 0)
		return (KEY_CTRL_LEFT);
	else if (ft_strcmp(buff, KEY_CTRL_RIGHT_CODE) == 0)
		return (KEY_CTRL_RIGHT);
	return (0);
}

void			ft_insert_in_line(char *line, char *buff, int index)
{
	char tmp[ft_strlen(line) - index + 1]; //VLA

	tmp[ft_strlen(line) - index] = '\0';
	ft_strcpy(tmp, &line[index]);
	ft_strcpy(&line[index], buff);
	ft_strcpy(&line[index + ft_strlen(buff)], tmp);
}

void			init_line(t_line *tline, char *line, size_t allocsize, int index)
{
	tline->line = line;
	tline->alloczise = allocsize;
	tline->index = index;
}

int				last_word_index(t_line *line)
{
	char	*str;
	int		index;

	str = line->line;
	index = line->index;
	while (index > 0 && !ft_isalnum(str[index - 1]))
		--index;
	while (index > 0 && ft_isalnum(str[index - 1]))	
		--index;
	return (index);
}

int				next_word_index(t_line *line)
{
	char	*str;
	int		index;

	str = line->line;
	index = line->index;
	while (str[index] && ft_isalnum(str[index]))
		++index;
	while (str[index] && !ft_isalnum(str[index]))	
		++index;
	return (index);
}